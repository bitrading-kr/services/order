const mongoose = require('mongoose'),
      axios = require('axios'),
      OrderManager = require('@bitrading-worker/order-manager'),
      mergeOptions = require('merge-options'),
      moment = require('moment'),
      { Url, resolve, format } = require('url'),
      { decode, sign, verify } = require('jsonwebtoken'),
      _ = require('lodash');

const WorkerSchema =  mongoose.Schema({
  id: { type: String, required: true, unique: true },
  auth: {
    access_key: { type: String },
    secret_key: { type: String }
  },
  node: {
    protocol: { type: String },
    host: { type: String },
    port: { type: String },
    root: { type: String }
  }
});

const AuthSchema =  mongoose.Schema({
  node: {
    protocol: { type: String },
    host: { type: String },
    port: { type: String },
    root: { type: String }
  }
});

class Order {
  constructor (host, port, user, password, db='admin') {
    this.connect(host, port, user, password, db)
      .then(res => {
        this.db = res;
      })
      .catch(err => {
        console.log(err);
      });
  }

  node2Url (node) {
    let nodeUrl = new Url();
    nodeUrl.protocol = _.get(node, 'node.protocol');
    nodeUrl.host = _.get(node, 'node.host') + ':' + _.get(node, 'node.port');
    let root = _.get(node, 'node.root') || '/';
    nodeUrl.pathname = root.slice(-1) == '/' ? root : root + '/';

    return format(nodeUrl);
  }

  async connect (host, port, user, password, db='admin') {
    let uri = `mongodb://${user}:${password}@${host}:${port}/${db}`;
    return await mongoose.connect(uri, { useNewUrlParser: true });
  }

  async getUserIdByToken (userAuthToken) {
    try {
      let authUrl = await this.getAuthUrl(),
          auth_login_url = resolve(authUrl, 'auth/login');
      let res = await axios.get(auth_login_url, {
        headers: {
          'user-auth-token': userAuthToken
        }
      });
      if (res.data == true) {
        return _.get(decode(userAuthToken), 'id');
      } else {
        return null;
      }
    } catch (err) {
      return null;
    }
  }

  async getWorkerUrl (user_id) {
    let node = await this.worker.findOne({id: user_id}, "node");
    return this.node2Url(node);
  }

  async genWorkerToken (user_id) {
    let authKey = await this.worker.findOne({id: user_id}, "auth");
    let { access_key, secret_key } = _.get(authKey, 'auth');
    return sign({
      id: user_id,
      access_key: access_key
    }, secret_key);
  }

  async getAuthUrl () {
    let nodes = await this.auth.find();
    let node = _.shuffle(nodes)[0];

    return this.node2Url(node);
  }

  async requestWorker (userAuthToken, method, urlPathname, data={}) {
    try {
      let userId = await this.getUserIdByToken(userAuthToken);
      if (userId == null) throw new Error(`유효하지 않는 사용자 토큰입니다.`);
      let workerUrl = await this.getWorkerUrl(userId),
          workerToken = await this.genWorkerToken(userId);

      if (workerUrl == null) throw new Error(`유효하지 않은 worker 주소입니다.`);
      if (workerToken == null) throw new Error(`유효하지 않은 worker 토큰입니다.`);

      let headerData = {
        'worker-token': workerToken
      };

      let reqUrl = resolve(workerUrl, urlPathname);
      switch (method) {
      case 'get':
      case 'delete':
      case 'head':
      case 'options':
        return await axios[method](reqUrl, {
          headers: headerData,
          params: data
        });
        break;
      case 'post':
      case 'put':
      case 'patch':
        return await axios[method](reqUrl, data, {
          headers: headerData
        });
        break;
      default:
        break;
      }
    } catch (err) {
      throw err;
    }
  }

  get worker () {
    if (this._worker == null) {
      this._worker = mongoose.model('worker', WorkerSchema);
    }
    return this._worker;
  }

  get auth () {
    if (this._auth == null) {
      this._auth = mongoose.model('auth', AuthSchema);
    }
    return this._auth;
  }

  /**
   * Use in router
   */
  /** orders */
  async setOrderData (userAuthToken, orderData) {
    let userId = await this.getUserIdByToken(userAuthToken);
    if (userId == null) throw new Error(`유효하지 않는 사용자 토큰입니다.`);

    orderData.entry.strategy = orderData.entry.strategy.map(elem => {
      elem.tradings = [];
      return elem;
    });
    orderData.target.strategy = orderData.target.strategy.map(elem => {
      elem.tradings = [];
      return elem;
    });
    if (orderData.stoploss.type != 0) {
      orderData.stoploss.strategy.tradings = [];
    }

    return mergeOptions({
      version: '1',
      user_id: userId,
      created_at: moment().format('YYYY-MM-DD HH:mm:ss.SSS Z'),
      title: '',
      desc: '',
      author: '',
      sourceId: '-1'
    }, orderData);
  }

  async addOrder (userAuthToken, orderData) {
    try {

      // ordermanager 생성
      let om;
      try {
        orderData = await this.setOrderData(userAuthToken, orderData);
        om = new OrderManager([orderData]);
      } catch (err) {
        console.log(err);
        throw new TypeError(`Order data validator test was failed`);
      }
      let result = await this.requestWorker(userAuthToken, 'post', 'orders', om.orders[0].order);
      return result.data;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async updateOrder (userAuthToken, orderId, orderData) {
    try {
      let result = await this.requestWorker(userAuthToken, 'put', 'orders/' + orderId, orderData);
      return result.data;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getOrders (userAuthToken, orderId=null) {
    try {
      if (orderId == null) {
        let result = await this.requestWorker(userAuthToken, 'get', 'orders');
        let om = new OrderManager(result.data);
        return om.orders.map(elem => elem.order);
      } else {
        let result = await this.requestWorker(userAuthToken, 'get', 'orders/' + orderId);
        let om = new OrderManager([result.data]);
        return om.orders[0].order;
      }
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async deleteOrder (userAuthToken, orderId) {
    try {
      let result = await this.requestWorker(userAuthToken, 'delete', 'orders/' + orderId);
      return result.data;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async activateOrder (userAuthToken, orderId) {
  }

  async activateOrder (userAuthToken, orderId) {
  }

  /** tradings */
  async getTradings (userAuthToken, tradingId=null, params /* use only /tradings */) {
    try {
      if (tradingId == null) {
        let result = await this.requestWorker(userAuthToken, 'get', 'tradings', params);
        return result.data;
      } else {
        let result = await this.requestWorker(userAuthToken, 'get', 'tradings/' + tradingId);
        return result.data;
      }
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async cancelTrading (userAuthToken, tradingId) {
  }

  /** Apis */
  async updateApi (userAuthToken, exchange, apiObj) {
    try {
      return await this.requestWorker(userAuthToken, 'put', 'apis/' + _.toLower(exchange), apiObj);
    } catch (err) {
      throw err;
    }
  }

  async getApi (userAuthToken, exchange) {
    try {
      let res =  await this.requestWorker(userAuthToken, 'get', 'apis/' + _.toLower(exchange)),
          key = _.get(res, 'data');
      if (typeof key === 'string') {
        if (key != '') {
          key = key.slice(0, 5) + '...' + key.slice(-5);
        }
      }
      return key;
    } catch (err) {
      throw err;
    }
  }

  async deleteApi (userAuthToken, exchange) {
    try {
      return await this.requestWorker(userAuthToken, 'delete', 'apis/' + _.toLower(exchange));
    } catch (err) {
      throw err;
    }
  }

  /** States */
  async getStates (userAuthToken) {
    try {
      return await this.requestWorker(userAuthToken, 'get', 'states');
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new Order(process.env.DB_HOST, process.env.DB_PORT, process.env.DB_USERNAME, process.env.DB_PASSWORD, process.env.DB_DATABASE);
