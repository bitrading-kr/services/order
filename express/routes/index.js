var express = require('express');
var router = express.Router();
const order = require('../middleware/Order.js');
const _ = require('lodash');
const axios = require('axios');
/* GET home page. */
function getAuthToken (headers) {
  let token = _.get(headers, 'user-auth-token');
  if (token == null) throw new Error("ERROR");
  return token;
}

async function handler (req, res, func) {
  try {
    let token = getAuthToken(req.headers);
    let result = await func(token, _.get(req, 'params'), _.get(req, 'body'));
    res.send(result).end();
  } catch (err) {
    res.status(500).send(err.message).end();
  }
}

router.route('/orders')
  .get(async function(req, res, next) {
    handler(req, res, async function (token, param, data) {
      let result = await order.getOrders(token);
      return result;
    });
  })
  .post(async function (req, res, next) {
    handler(req, res, async function (token, param, data) {
      let result = await order.addOrder(token, data);
      return result;
    });
  });

router.route('/orders/:id')
  .get(function(req, res, next) {
    handler(req, res, async function (token, {id}, data) {
      let result = await order.getOrders(token, id);
      return result;
    });
  })
  .put(function(req, res, next) {
    handler(req, res, async function (token, {id}, data) {
      // let result = await order.getOrders(token, id);
      // return result;
    });
  })
  .delete(function(req, res, next) {
    handler(req, res, async function (token, {id}, data) {
      let result = await order.deleteOrder(token, id);
      return result;
    });
  });

/**
 * tradings
 */
router.get('/tradings', function (req, res, next) {
  let query = req.query;
  handler(req, res, async function (token, params, data) {
    let result = await order.getTradings(token, null, query);
    return result;
  });
});

router.get('/tradings/:id', function (req, res, next) {
  handler(req, res, async function (token, {id}, data) {
    let result = await order.getTradings(token, id);
    return result;
  });
});

/**
 * commands
 */
router.put('/commands/orders/:id', function (req, res, next) {
  handler(req, res, async function (token, { id }, data) {
    let command = data.command;
    let dataQuery = {};
    switch (command) {
    case 'active':
      dataQuery.state = 'active';
      break;
    case 'inactive':
      dataQuery.state = 'inactive';
      break;
    }
    let result = await order.updateOrder(token, id, dataQuery);
    return result;
  });
});

router.get('/commands/tradings', function (req, res, next) {
});

router.get('/db', async function(req, res, next) {
  try {
    // res.send("Mongo DB is connected " + process.env.DB_USERNAME).end();
  } catch (e) {
    res.send("Mongo DB is not connected..." + e).end();
  }
});

/**
 * apis
 */
router.route('/apis/:exchange')
  .get(function (req, res, next) {
    handler(req, res, async function (token, {exchange}, data) {
      return await order.getApi(token, exchange);
    });
  })
  .put(async function (req, res, next) {
    handler(req, res, async function (token, {exchange}, data) {
      let api = await order.updateApi(token, exchange, {
        access_key: data.access_key,
        secret_key: data.secret_key
      });
      return api.ok == 1;
    });
  })
  .delete(async function (req, res, next) {
    handler(req, res, async function (token, {exchange}, data) {
      await order.deleteApi(token, exchange);
      return true;
    });
  });

/**
 * states
 */
router.get('/states', function (req, res, next) {
  handler(req, res, async function (token, param, data) {
    let result = await order.getStates(token);
    return result.data;
  });
});

module.exports = router;
