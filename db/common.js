db.createCollection('workers'); // worker node
db.createCollection('auths'); // auth node

// worker list
/** worker 1 */
db.workers.insert({
  "id": "tester_sam",
  auth: {
    access_key: '57AeH3aUPpo6mFrVD1XckV1a6SLtFi2zHc2KoW93fsyQTCeE8',
    secret_key: '2kJ6d4yHwBTRk62pamqG4sW8BPweQStpqaapPYMwTYXoDmUkdk'
  },
  "node" : { "protocol" : "https", "host" : "35.243.97.227", "port" : "443", "root" : "/worker1" }
});
db.workers.insert({
  id: 'tester_red',
  auth: {
    access_key: '2X4MxHyCVP9KrrhVH8ykmHSxYgy2pJ56ZGMjr4AM9f9EmXKcwC',
    secret_key: '2PEEiAyGZGPeVJ2ey4UnZyEzzvsvJdoc6wsxc1Zx35MyzEToET'
  },
  "node" : { "protocol" : "https", "host" : "35.243.97.227", "port" : "443", "root" : "/worker2" }
});
db.workers.insert({
  id: 'tester_mint',
  auth: {
    access_key: '2GenXWPFQJ2Qo2aGouCv4aAkM8Rb8oZAXYF3NBUhsWe1yHikNV',
    secret_key: 'hMMrBwRtCsnxsNDjXLxppuCT8i9hN2BYdB1BGUhA3y4RL1v5J'
  },
  "node" : { "protocol" : "https", "host" : "35.243.97.227", "port" : "443", "root" : "/worker3" }
});
db.workers.insert({
  id: 'tester_h22',
  auth: {
    access_key: 'JqAnFKbbSNbzA8n6CYynBg95cCz6fQMjCoUdWSeEPb88LVoDg',
    secret_key: 'saMyUUUiE4qKUxhhe199gVjP3v1QR2MzYCkZBekV5FtEzX6Zo'
  },
  "node" : { "protocol" : "https", "host" : "35.243.97.227", "port" : "443", "root" : "/worker4" }
});
db.workers.insert({
  id: 'tester_love9',
  auth: {
    access_key: '2g71Ayi7txSFP3aJjmRePStDAHGrmHtYiDJFUMucKwSEG6bLJh',
    secret_key: 'WuQQXFijSSR3fBawzZeH9N9pf3NroQCEefR1mQHMayNPgU362'
  },
  "node" : { "protocol" : "https", "host" : "35.243.97.227", "port" : "443", "root" : "/worker5" }
});

/** worker admin */
db.workers.insert({
  "id": "tester_dk",
  auth: {
    access_key: '2FhEmqFQGv58VKtCuBR5RWYJgQxAophoTHbXKmfPivmzZXTKVs',
    secret_key: 'kaRajxrP4qjCUkASAqX4EVvTiL7WuqbjFwehw1WwovyqVJCCW'
  },
  "node" : { "protocol" : "https", "host" : "35.201.178.45", "port" : "443", "root" : "/worker1" }
});
db.workers.insert({
  id: 'tester_isoo',
  auth: {
    access_key: '2e91itkTY1YYfS3vH3HmbU3XLrRRjEuyw9Yy4HVuHHAhEap2fr',
    secret_key: '23NG3ELMtiS3gpzn9eaP3G22ai8vp27M9WVmGYdsmJJcFkNA3s'
  },
  "node" : { "protocol" : "https", "host" : "35.201.178.45", "port" : "443", "root" : "/worker2" }
});
db.workers.insert({
  id: 'tester_edg2',
  auth: {
    access_key: 'bt3CAMAjA9rfpg9d5bBRHa9GcPa7PXvRfjvrxTtGJD87RvZ8P',
    secret_key: 'JsQmDGG8DjTJ48Bu1VrMCEeViEGEjiCPzEmuWSGQEppWUWShD'
  },
  "node" : { "protocol" : "https", "host" : "35.201.178.45", "port" : "443", "root" : "/worker3" }
});
db.workers.insert({
  id: 'tester_hh',
  auth: {
    access_key: '2TkKmazMTA2LEkZ7mabXFcjkQ7Dh2XR6heJz7YFRN5ZkiqWSXu',
    secret_key: '261d6pHk4nHR746NJMyfs9PiD78i197WErdYhrVK4eMTgR4JYJ'
  },
  "node" : { "protocol" : "https", "host" : "35.201.178.45", "port" : "443", "root" : "/worker4" }
});
db.workers.insert({
  id: 'tester_admin',
  auth: {
    access_key: '7cGxSVpqDQKGRBszEfcEhSoJsXXLCxVQoKzmPddrkP3M7fpYY',
    secret_key: '2e33H5FraRcmaC2dQSvqmStjErYjWQLasVkJbXzMwx7wsYV3Ze'
  },
  "node" : { "protocol" : "https", "host" : "35.201.178.45", "port" : "443", "root" : "/worker5" }
});


// auth list
db.auths.insert({
  "node" : { "protocol" : "https", "host" : "35.221.82.146", "port" : "443", "root" : "/" }
});
